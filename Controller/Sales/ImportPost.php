<?php

namespace CustomGateway\TradePortal\Controller\Sales;

use Magento\Framework\App\Action\Context;

class ImportPost
	extends \Magento\Framework\App\Action\Action
{
	private $_graphql = null;
	private $_sessionModel = null;

	public function __construct(
		\CustomGateway\TradePortal\Helper\GraphQl $graphQl,
		\Magento\Customer\Model\Session $sessionModel,
        Context $context)
	{
		parent::__construct($context);

		$this->_graphQl = $graphQl;
		$this->_sessionModel = $sessionModel;
	}

	public function execute()
	{
		$test = $this->getRequest()->getPost('test');

		$file = $this->getRequest()->getFiles()['upload'];

		$contents = file_get_contents($file['tmp_name']);

		try
		{
		    $this->_importOrders($contents, $test);

		    $this->messageManager->addSuccessMessage(__('Your orders were imported.'));

		    $url = $this->_buildUrl('sales/order/history', [ '_secure' => true ]);

		    return $this->resultRedirectFactory->create()->setUrl($this->_redirect->success($url));
		}
		catch(\Exception $e)
		{
			$this->messageManager->addErrorMessage($e->getMessage());

		    $url = $this->_buildUrl('*/*/import', [ ]);

		    return $this->resultRedirectFactory->create()->setUrl($this->_redirect->error($url));
		}


	}

	private function _importOrders(string $fileContents, $test)
	{

		$customer = $this->_sessionModel->getCustomer();

		$retailerRefId = $customer->getDataModel()->getCustomAttribute('cg_tp_retailer_ref_id')->getValue();
		$clientId = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_id')->getValue();
		$clientSecret = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_secret')->getValue();

		$this->_graphQl->login($clientId, $clientSecret, [
			'v2.trade-portal.retailer'
		]);

		$query = '
			mutation($retailerRefId: ID!, $file: Base64!, $test: Boolean) {
				importOrders(retailerRefId: $retailerRefId, file: $file, test: $test) {
					id
				}
			}';

		$this->_graphQl->query($query, [
			'retailerRefId' => $retailerRefId,
			'file'			=> base64_encode($fileContents),
			'test'			=> $test ? true : false
		]);

	}

	private function _buildUrl($route = '', $params = [])
    {
        $urlBuilder = $this->_objectManager->create(\Magento\Framework\UrlInterface::class);

        return $urlBuilder->getUrl($route, $params);
    }
}
