<?php

namespace CustomGateway\TradePortal\Controller\Sales;

use Magento\Framework\App\Action\Context;

class Reorder
	extends \Magento\Framework\App\Action\Action
{
	private $_graphql = null;
	private $_sessionModel = null;

	public function __construct(
		\CustomGateway\TradePortal\Helper\GraphQl $graphQl,
		\Magento\Customer\Model\Session $sessionModel,
        Context $context)
	{
		parent::__construct($context);

		$this->_graphQl = $graphQl;
		$this->_sessionModel = $sessionModel;
	}

	public function execute()
	{
		$this->_login();

		$query = '
			mutation($id: ID!) {
				duplicateOrder(id: $id) {
					id
				}
			}';

		$result = $this->_graphQl->query($query, [
			'id' => $this->getRequest()->getParam('id')
		])['data']['duplicateOrder'];

		$this->messageManager->addSuccessMessage(__('Order was successfully duplicated.'));

		$url = $this->_buildUrl('tradeportal/sales/vieworder', [ '_secure' => true, 'id' => $result['id']]);

		return $this->resultRedirectFactory->create()->setUrl($this->_redirect->success($url));
	}

	private function _login()
	{
		$customer = $this->_sessionModel->getCustomer();

		$clientId = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_id')->getValue();
		$clientSecret = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_secret')->getValue();

		$this->_graphQl->login($clientId, $clientSecret, [
			'v2.trade-portal.retailer'
		]);
	}

	private function _buildUrl($route = '', $params = [])
    {
        $urlBuilder = $this->_objectManager->create(\Magento\Framework\UrlInterface::class);

        return $urlBuilder->getUrl($route, $params);
    }
}

