<?php

namespace CustomGateway\TradePortal\Controller\Sales;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Import extends \Magento\Framework\App\Action\Action
{
	protected $resultPageFactory;

	public function __construct(
        Context $context,
        PageFactory $resultPageFactory)
	{
        $this->resultPageFactory = $resultPageFactory;

        parent::__construct($context);
    }

	public function execute()
	{
        $resultPage = $this->resultPageFactory->create();

        $resultPage->getConfig()->getTitle()->set(__('Import Orders'));

        $block = $resultPage->getLayout()->getBlock('customer.account.link.back');

        if ($block)
		{
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }

        return $resultPage;
	}
}

