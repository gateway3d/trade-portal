<?php

namespace CustomGateway\TradePortal\Controller\Sales;

use Magento\Sales\Controller\OrderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class CreatePost
	extends \Magento\Framework\App\Action\Action
{
	private $_graphql = null;
	private $_sessionModel = null;
	private $_regionFactory = null;

	public function __construct(
		\CustomGateway\TradePortal\Helper\GraphQl $graphQl,
		\Magento\Customer\Model\Session $sessionModel,
		\Magento\Directory\Model\RegionFactory $regionFactory,
        Context $context)
	{
		parent::__construct($context);

		$this->_graphQl = $graphQl;
		$this->_sessionModel = $sessionModel;
		$this->_regionFactory = $regionFactory;
	}

	public function execute()
	{
		$data = $this->getRequest()->getPost();
		$test = $this->getRequest()->getPost('test');

		$customer = $this->_sessionModel->getCustomer();
		$billing = $customer->getDefaultBillingAddress();

		if(!$billing)
		{
			$this->messageManager->addErrorMessage("Please ensure you have set a default billing address");

			$url = $this->_buildUrl('*/*/create', [ ]);

			return $this->resultRedirectFactory->create()->setUrl($this->_redirect->error($url));
		}

		$billing = $billing->getDataModel();

		$order = [
			"external_ref"			=> $data->external_ref,

			"shipping_carrier"		=> explode('|', $data->shipping_method)[0],
			"shipping_method"		=> explode('|', $data->shipping_method)[1],

			"customer_name"			=> $data->customer_name,
			"shipping_company"		=> $data->shipping_company,
			"shipping_address_1"	=> $data->shipping_address_1,
			"shipping_address_2"	=> $data->shipping_address_2,
			"shipping_address_4"	=> $data->shipping_address_4,
			"shipping_address_5"	=> $this->_getRegion($data),
			"shipping_postcode"		=> $data->shipping_postcode,
			"shipping_country_code" => $data->shipping_country_code ?? 'GB',

			"billing_customer_name"	=> implode(' ', array_filter([ $billing->getFirstname(), $billing->getLastname() ])),
			"billing_address_1"		=> implode(' ', array_filter($billing->getStreet())),
			"billing_address_4"		=> $billing->getCity(),
			"billing_address_5"		=> $billing->getRegion()->getRegionCode(),
			"billing_postcode"		=> $billing->getPostcode(),

			"items" => [
				[
					"sku"			=> $data->sku,
					"quantity"		=> intval($data->quantity),
					"type"			=> 1,
					"external_url"	=> $data->external_url,
					"external_thumbnail_url" => $data->external_url
				]
			],

			"attributes" => $this->_getCustomAttributes($data)
		];

		if ($data->hs_code)
		{
			$hsCode = [
				"name" => ":HSCODE",
				"value" => $data->hs_code
			];
			$order["items"][0]["attributes"][] = $hsCode;
		}

		if ($data->declared_value)
		{
			$declaredValue = [
				"name" => ":DECLARED_VALUE",
				"value" => $data->declared_value
			];
			$order["items"][0]["attributes"][] = $declaredValue;
		}

		try
		{
			$this->_createOrder($order, $test);

			$this->messageManager->addSuccessMessage(__('Your order was imported.'));

			$url = $this->_buildUrl('sales/order/history', [ '_secure' => true ]);

			return $this->resultRedirectFactory->create()->setUrl($this->_redirect->success($url));
		}
		catch(\Exception $e)
		{
			$this->_sessionModel->setOrderData($data);

			$this->messageManager->addErrorMessage($e->getMessage());

			$url = $this->_buildUrl('*/*/create', [ ]);

			return $this->resultRedirectFactory->create()->setUrl($this->_redirect->error($url));
		}
	}

	private function _getRegion(\Zend\Stdlib\Parameters $data): string
	{
		if($data->region_id)
		{
			return $this->_regionFactory->create()->load($data->region_id)->getName();
		}
		else
		{
			return $data->region;
		}
	}

	private function _getCustomAttributes(\Zend\Stdlib\Parameters $data): array
	{
		$_attributes = [];

		if($data['attributes'])
		{
			foreach($data['attributes'] as $key => $value)
			{
				$_attributes[] = [
					'name' => $key,
					'value' => $value
				];
			}
		}

		return $_attributes;
	}

	private function _createOrder(array $order, $test)
	{
		$customer = $this->_sessionModel->getCustomer();

		$retailerRefId = $customer->getDataModel()->getCustomAttribute('cg_tp_retailer_ref_id')->getValue();
		$clientId = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_id')->getValue();
		$clientSecret = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_secret')->getValue();

		$this->_graphQl->login($clientId, $clientSecret, [
			'v2.trade-portal.retailer'
		]);

		$query = '
			mutation($retailerRefId: ID!, $orders: [OrderInput]!, $test: Boolean) {
				createOrders(retailerRefId: $retailerRefId, orders: $orders, test: $test) {
					id
				}
			}';

		$this->_graphQl->query($query, [
			'retailerRefId' => $retailerRefId,
			'orders' => [ $order ],
			'test'	=> $test ? true : false
		]);
	}

	private function _buildUrl($route = '', $params = [])
    {
        $urlBuilder = $this->_objectManager->create(\Magento\Framework\UrlInterface::class);

        return $urlBuilder->getUrl($route, $params);
    }
}
