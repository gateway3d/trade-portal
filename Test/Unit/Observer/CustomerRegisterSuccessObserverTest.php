<?php

namespace CustomGateway\TradePortal\Test\Unit\Observer;

final class CustomerRegisterSuccessObserverTest
	extends \PHPUnit\Framework\TestCase
{
	private $_customerModelMock = null;
	private $_addressModelMock = null;
	private $_scopeConfigMock = null;
    private $_customerLoginSuccessObserver = null;

    public function setUp()
    {
		$this->_customerModelMock = $this->createMock(\Magento\Customer\Model\Customer::class);

		$this->_addressModelMock = $this->createMock(\Magento\Customer\Model\Address::class);

		$this->_scopeConfigMock = $this->createMock(\Magento\Framework\App\Config\ScopeConfigInterface::class);

		$this->_customerRegisterSuccessObserver = new \CustomGateway\TradePortal\Observer\CustomerRegisterSuccessObserver(
			$this->_customerModelMock,
			$this->_addressModelMock,
			$this->_scopeConfigMock,
			new \GuzzleHttp\Client,
			new \Psr\Log\NullLogger
        );
    }

	public function testExecute()
	{
		$observerMock = $this->createMock(\Magento\Framework\Event\Observer::class);

		$customerData = $this->createMock(\Magento\Customer\Model\Data\Customer::class);

		$observerMock
			->expects($this->once())
			->method('getData')
			->with('customer')
			->will($this->returnValue($customerData));

		$this->_scopeConfigMock
			->method('getValue')
			->will($this->returnCallback(function($key)
			{
				switch($key)
				{
					case 'custom_gateway_trade_portal/advanced/oauth_url':
						return 'https://oauth.custom-gateway.red/token';

					case 'custom_gateway_trade_portal/advanced/api_url':
						return 'https://whitelabel.custom-gateway.red/trade-portal/graphql';

					case 'custom_gateway_trade_portal/oauth_credentials/client_id':
						return '4fd42999-5a25-470a-a584-5e00d5899f55';

					case 'custom_gateway_trade_portal/oauth_credentials/client_secret':
						return 'test';

					case 'custom_gateway_trade_portal/general/supplier_ref_ids':
						return '17538';

					default:
						die($key);
				}
			}));
				
		$this->_customerRegisterSuccessObserver->execute($observerMock);
	}
}

