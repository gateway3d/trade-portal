<?php

namespace CustomGateway\TradePortal\Helper;

final class GraphQl
{
	private $_guzzle = null;
	private $_scopeConfig = null;
	private $_accessToken = '';

	public function __construct(
		\GuzzleHttp\Client $guzzle,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
	{
		$this->_guzzle = $guzzle;
		$this->_scopeConfig = $scopeConfig;
	}

	public function login(string $clientId, string $clientSecret, array $scopes = []): string
	{
		try
		{
			$url = $this->_scopeConfig->getValue('custom_gateway_trade_portal/advanced/oauth_url');

			$response = $this->_guzzle->post($url, [
				'form_params' => [
					'client_id'		=> $clientId,
					'client_secret' => $clientSecret,
					'grant_type'	=> 'client_credentials',
					'scope'			=> implode(' ', $scopes)
				],

				'verify' => false
			]);

			$json = (string)$response->getBody();

			$data = json_decode($json, true);

			$this->_accessToken = $data['access_token'];

			return $data['access_token'];
		}
		catch(\GuzzleHttp\Exception\BadResponseException $e)
		{
			$json = (string)$e->getResponse()->getBody();

			$data = json_decode($json, true);

			if(isset($data['error']['message']))
			{
				throw new \Exception($data['error']['message']);
			}
			else
			{
				throw new \Exception('Login failed');
			}
		}
	}

	public function query(string $query, array $variables): array
	{
		try
		{
			$url = $this->_scopeConfig->getValue('custom_gateway_trade_portal/advanced/api_url');

			$response = $this->_guzzle->post($url, [
				'headers' => [
					"Authorization" => "Bearer {$this->_accessToken}"
				],

				'json' => [
					'query' => $query,

					'variables' => $variables
				],

				'verify' => false
			]);

			$json = (string)$response->getBody();

			$data = json_decode($json, true);

			if(isset($data['errors']))
			{
				if($data['errors'][0]['subErrors'] ?? [])
				{
					throw new \Exception($data['errors'][0]['subErrors'][0]['message']);
				}
				else
				{
					throw new \Exception($data['errors'][0]['message']);
				}
			}
		}
		catch(\GuzzleHttp\Exception\BadResponseException $e)
		{
			$json = (string)$e->getResponse()->getBody();

			$data = json_decode($json, true);

			if(isset($data['error']['message']))
			{
				throw new \Exception("Query failed: {$data['error']['message']}");
			}
			else
			{
				throw new \Exception('Query failed');
			}
		}
		
		return $data;
	}
}

