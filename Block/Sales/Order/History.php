<?php

namespace CustomGateway\TradePortal\Block\Sales\Order;

final class History
	extends \Magento\Framework\View\Element\Template
{
	protected $_template = 'CustomGateway_TradePortal::order/history.phtml';

	private $_sessionModel = null;
	private $_graphQl = null;
	private $_error = null;

	public function __construct(
		\Magento\Framework\App\Request\Http $request,
		\Magento\Customer\Model\Session $sessionModel,
		\CustomGateway\TradePortal\Helper\GraphQl $graphQl,
		\Magento\Framework\View\Element\Template\Context $context,
		array $data = [])
	{
		parent::__construct($context, $data);

		$this->_request = $request;
		$this->_sessionModel = $sessionModel;
		$this->_graphQl = $graphQl;
	}

	public function getPath()
    {
        return $this->_getData('path') ?: '*/*/*';
    }

	public function getError()
	{
		return $this->_error;
	}

	public function getOrders()
	{
		return $this->_getOrders();
	}

	public function getViewUrl(array $order)
	{
		return $this->getUrl('tradeportal/sales/vieworder', [
			'id' => $order['id']
		]);
	}

	protected function _prepareLayout()
	{
		parent::_prepareLayout();
	
		try
		{
			$this->_login();
		}
		catch(\Exception $e)
		{
			$this->_error = $e;
		}

		return $this;
	}

	private function _login()
	{
		$customer = $this->_sessionModel->getCustomer();

		$clientId = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_id')->getValue();
		$clientSecret = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_secret')->getValue();

		$this->_graphQl->login($clientId, $clientSecret, [
			'v2.trade-portal.retailer',
			'order-it.order-manager.view'
		]);
	}

	private function _getOrders(): array
	{
		$customer = $this->_sessionModel->getCustomer();

		$retailerRefId = $customer->getDataModel()->getCustomAttribute('cg_tp_retailer_ref_id')->getValue();

		$query = '
			query($filter: Json!, $order: Json!, $count: Int!, $page: Int!) {
				orders(filter: $filter, order: $order, count: $count, page: $page) {
					pages {
						pageCount
						current
						firstItemNumber
						lastItemNumber
						totalItemCount
					}

					items {
						id
						external_ref
						status_name
						ref
						customer_name
						shipping_address_1
						creation_datetime
						has_error
						error_message
					}
				}
			}';

		return $this->_graphQl->query($query, [
			'filter' => [
				'company_ref_id' => $retailerRefId
			],

			'order' => [
				'id' => 'DESC'
			],

			'count' => intval($this->_request->getParam('limit') ?: 50),
			'page' => intval($this->_request->getParam('p') ?: 1)
		]);
	}
}

