<?php

namespace CustomGateway\TradePortal\Block\Sales;

use Magento\Customer\Model\Context;

class ViewOrder
	extends \Magento\Directory\Block\Data
{
	private $_sessionModel = null;
	private $_graphQl = null;
	private $_order = null;
	private $_error = null;

	public function __construct(
		\Magento\Customer\Model\Session $sessionModel,
		\CustomGateway\TradePortal\Helper\GraphQl $graphQl,
		\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
		array $data = [])
	{
		parent::__construct($context, $directoryHelper, $jsonEncoder, $configCacheType, $regionCollectionFactory, $countryCollectionFactory, $data);

		$this->_sessionModel = $sessionModel;
		$this->_graphQl = $graphQl;
	}

	public function getError()
	{
		return $this->_error;
	}

	public function getOrder()
	{
		return $this->_order;
	}

	public function getReorderUrl(array $order)
	{
		return $this->getUrl('tradeportal/sales/reorder', [
			'id' => $order['id']
		]);
	}

	protected function _prepareLayout()
	{
		parent::_prepareLayout();

		try
		{
			$this->_login();

			$this->_order = $this->_getOrder();

	        $this->pageConfig->getTitle()->set(__('Order #%1', $this->_order['external_ref']));
		}
		catch(\Exception $e)
		{
			$this->_error = $e;
		}

		return $this;
	}

	private function _getOrder()
	{
		$query = '
			query($id: ID!) {
				order(id: $id) {
					id
					external_ref

					sale_datetime
					creation_datetime

					billing_customer_name
					billing_address_1
					billing_address_2
					billing_address_3
					billing_address_4
					billing_address_5
					billing_postcode

					customer_name
					shipping_address_1
					shipping_address_2
					shipping_address_3
					shipping_address_4
					shipping_address_5
					shipping_postcode
					shipping_country_code

					shipping_method
					shipping_carrier

					status
					status_name

					has_error
					error_message
					
					items {
						items {
							thumbnail {
								url
							}
	
							description
							sku
							mapped_sku
							quantity
						}
					}
				}
			}';

		return $this->_graphQl->query($query, [
			'id' => $this->_request->getParam('id')
		])['data']['order'];
	}

	private function _login()
	{
		$customer = $this->_sessionModel->getCustomer();

		$clientId = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_id')->getValue();
		$clientSecret = $customer->getDataModel()->getCustomAttribute('cg_tp_oauth_client_secret')->getValue();

		$this->_graphQl->login($clientId, $clientSecret, [
			'order-it.order-manager.view'
		]);
	}

}
