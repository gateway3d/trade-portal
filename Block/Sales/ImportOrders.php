<?php

namespace CustomGateway\TradePortal\Block\Sales;

class ImportOrders
	extends \Magento\Directory\Block\Data
{
	public function getActionUrl()
	{
		return $this->_urlBuilder->getUrl(
			'tradeportal/sales/importpost',
			[ '_secure' => true ]);
	}
}

