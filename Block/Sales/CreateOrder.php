<?php

namespace CustomGateway\TradePortal\Block\Sales;

class CreateOrder
	extends \Magento\Directory\Block\Data
{
	private $_sessionModel = null;
	private $_shippingConfig = null;

	public function __construct(
		\Magento\Framework\App\Request\Http $request,
		\Magento\Customer\Model\Session $sessionModel,
		\Magento\Shipping\Model\Config $shippingConfig,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,		
		\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
		array $data = [])
	{
		parent::__construct($context, $directoryHelper, $jsonEncoder, $configCacheType, $regionCollectionFactory, $countryCollectionFactory, $data);

		$this->_sessionModel = $sessionModel;
		$this->_shippingConfig = $shippingConfig;
		$this->_scopeConfig = $scopeConfig;
	}

	public function getOrderData()
	{
		return $this->_sessionModel->getOrderData();
	}

	public function getShippingMethods()
	{
		$options = [ ];

		foreach($this->_getShippingMethods() as $method)
		{
			$options[] = [
				'value' => "{$method['carrierCode']}|{$method['methodCode']}",
				'label' => "{$method['carrierLabel']} - {$method['methodLabel']}"
			];
		}
		
		return $options;
	}

	public function getSaveUrl()
	{
		return $this->_urlBuilder->getUrl(
            'tradeportal/sales/createpost',
            [ '_secure' => true ]);
	}

	public function getCustomOrderAttributes()
	{
		$fields = $this->_scopeConfig->getValue('custom_gateway_trade_portal/advanced/custom_order_attributes');

		return $fields ? explode(',', $fields) : [];
	}

	private function _getShippingMethods()
	{
		$methods = [];

		$carriers = $this->_shippingConfig->getActiveCarriers();

		foreach($carriers as $carrierCode => $carrierModel)
		{
			$carrierLabel = $this->_scopeConfig->getValue("carriers/{$carrierCode}/title");

			foreach($carrierModel->getAllowedMethods() as $methodCode => $methodLabel)
			{
				$methods[] = [
					'carrierCode'	=> $carrierCode,
					'methodCode'	=> $methodCode,
					'carrierLabel'	=> $carrierLabel,
					'methodLabel'	=> $methodLabel
				];
			}
		}

		return $methods;
	}
}

