<?php

namespace CustomGateway\TradePortal\Block\Customer\Form;

class Register
	extends \Magento\Customer\Block\Form\Register
{
	private $_graphQl = null;

	public function __construct(
		\CustomGateway\TradePortal\Helper\GraphQl $graphQl,
		\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Url $customerUrl)
	{
		parent::__construct($context, $directoryHelper, $jsonEncoder, $configCacheType, $regionCollectionFactory, $countryCollectionFactory, $moduleManager, $customerSession, $customerUrl);

		$this->_graphQl = $graphQl;
	}

	protected function _prepareLayout()
	{
		$clientId = $this->getConfig('custom_gateway_trade_portal/oauth_credentials/client_id');
		$clientSecret = $this->getConfig('custom_gateway_trade_portal/oauth_credentials/client_secret');

		if(!$clientId || !$clientSecret)
		{
			throw new \Exception('Please ensure that Custom Gateway Trade Portal OAuth credentials are set');
		}

		$supplierRefIds = $this->getConfig('custom_gateway_trade_portal/general/supplier_ref_ids');

		if(!$supplierRefIds)
		{
			throw new \Exception('Please ensure that Custom Gateway Trade Portal supplier reference IDs have been configured');
		}

		$this->_graphQl->login($clientId, $clientSecret, [
			'v2.trade-portal.supplier'
		]);

		parent::_prepareLayout();
	}

	public function getShowAddressFields()
	{
		return true;
	}

	public function isNewsletterEnabled()
	{
		return false;
	}
}

