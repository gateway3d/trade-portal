<?php

namespace CustomGateway\TradePortal\Block\PersonaliseIt\Product\View;

class App
	extends \Gateway3D\PersonaliseIt\Block\Product\View\App
{
	private $_sessionFactory = null;

	public function __construct(
		\Magento\Customer\Model\SessionFactory $sessionFactory,
		\Magento\Catalog\Block\Product\Context $context,
		\Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        array $data = [])
	{
		$this->_sessionFactory = $sessionFactory;
		
		parent::__construct($context, $arrayUtils, $data);
	}

	protected function _construct()
	{
		if($this->_sessionFactory->create()->isLoggedIn())
		{
			parent::_construct();
		}
	}

	public function getTemplateFile($template = null)
	{
		return $this->resolver->getTemplateFileName('Gateway3D_PersonaliseIt::product/view/app.phtml');
	}

	protected function _getAdditionalFragmentParameters()
	{
		$additional = parent::_getAdditionalFragmentParameters();

		$customer = $this->_sessionFactory->create()->getCustomer();

		$attribute = $customer->getDataModel()->getCustomAttribute('cg_tp_retailer_ref_id');

		if($attribute)
		{
			$additional['guid'] = $attribute->getValue();

			return $additional;
		}
		else
		{
			throw new \Exception('Missing attribute for user');
		}
	}
}

