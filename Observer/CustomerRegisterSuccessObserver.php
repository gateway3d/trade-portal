<?php

namespace CustomGateway\TradePortal\Observer;

final class CustomerRegisterSuccessObserver
	implements \Magento\Framework\Event\ObserverInterface
{
	private $_integrationService = null;
	private $_oauthService = null;
	private $_storeManager = null;
	private $_customerModel = null;
	private $_addressModel = null;
	private $_scopeConfig = null;
	private $_graphQl = null;
	private $_logger = null;

	public function __construct(
		\Magento\Integration\Api\IntegrationServiceInterface $integrationService,
		\Magento\Integration\Api\OauthServiceInterface $oauthService,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Customer\Model\Customer $customerModel,
		\Magento\Customer\Model\Address $addressModel,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\CustomGateway\TradePortal\Helper\GraphQl $graphQl,
		\Psr\Log\LoggerInterface $logger)
	{
		$this->_integrationService = $integrationService;
		$this->_oauthService = $oauthService;
		$this->_storeManager = $storeManager;
		$this->_customerModel = $customerModel;
		$this->_addressModel = $addressModel;
		$this->_scopeConfig = $scopeConfig;
		$this->_graphQl = $graphQl;
		$this->_logger = $logger;
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$this->_customerData = $observer->getData('customer');

		$addressId = $this->_customerData->getDefaultBilling();

		if($addressId)
		{
			$address = $this->_addressModel->load($addressId);

			$company = $this->_addressModel->getCompany() ?? '';
		}
		else
		{
			$company = "";
		}

		$this->_login();

		$retailerCreds = $this->_createRetailer($company);

		$this->_customerData->setCustomAttribute('cg_tp_retailer_ref_id', $retailerCreds['company_ref_id']);
		$this->_customerData->setCustomAttribute('cg_tp_oauth_client_id', $retailerCreds['oauth_client_id']);
		$this->_customerData->setCustomAttribute('cg_tp_oauth_client_secret', $retailerCreds['oauth_client_secret']);

		$this->_customerModel->updateData($this->_customerData);
		$this->_customerModel->save();
	}

	private function _login()
	{
		$this->_graphQl->login(
			$this->_scopeConfig->getValue('custom_gateway_trade_portal/oauth_credentials/client_id'),
			$this->_scopeConfig->getValue('custom_gateway_trade_portal/oauth_credentials/client_secret'),
			[
				'v2.trade-portal.supplier'
			]);
	}

	private function _createRetailer(string $company): array
	{
		$supplierRefIds = $this->_scopeConfig->getValue('custom_gateway_trade_portal/general/supplier_ref_ids');

		$query = '
			mutation($info: RetailerInfoInput!) {
				createRetailer(info: $info) {
					company_ref_id
					oauth_client_id
					oauth_client_secret
				}
			}';

		$variables = [
			'info' => [
				'supplier_ref_ids' => array_map('intval', explode(',', $supplierRefIds)),
				'name' => $company,
				'contact_email' => $this->_customerData->getEmail(),

				'integration' => [
					'code' => 'magento2',

					'data' => $this->_getIntegrationData()
				],

				'settings' => $this->_getSettingsData()
			]
		];

		return $this->_graphQl->query($query, $variables)['data']['createRetailer'];
	}

	private function _getSettingsData()
	{
		$strategy = $this->_scopeConfig->getValue('custom_gateway_trade_portal/advanced/import_data_strategy');

		if($strategy)
		{		
			return [
				[
					'namespace'	=> 'whitelabel/trade-portal',
					'name'		=> 'import-data-strategy',
					'value'		=> $strategy
				],

				[
					'namespace' => 'api-sl-2-2',
					'name'		=> 'populate-mapped-sku-from-cpp',
					'value'		=> true
				]
			];
		}
	}

	private function _getIntegrationData()
	{
		$store = $this->_storeManager->getStore();
		$url = $store->getBaseUrl();
		$parts = parse_url($url);

		$integration = $this->_integrationService->findByName('Custom Gateway Trade Portal');

		if(!$integration)
		{
			throw new \Exception('Failed to find integration');
		}

		$token = $this->_oauthService->getAccessToken($integration->getConsumerId());

		if(!$token)
		{
			throw new \Exception('No access token');
		}

		return [
			'is_enabled'		=> true,
			'start_timestamp'	=> date('U'),

			'api_access_token'	=> $token->getToken(),
			'host'				=> $parts['host'],
			'url_root'			=> $parts['path'],
			'use_https'			=> $parts['scheme'] == 'https',
			'customer_id'		=> $this->_customerData->getId(),
			'store_id'			=> $store->getId(),
			'is_strict_address_enabled' => true,
			'import_status'		=> 'new'
		];
	}
}

