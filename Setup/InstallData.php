<?php

namespace CustomGateway\TradePortal\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
class InstallData implements InstallDataInterface
{
	private $_integrationService = null;
	private $_oauthService = null;
	private $_eavSetupFactory = null;
 
	public function __construct(
		\Magento\Integration\Api\IntegrationServiceInterface $integrationService,
		\Magento\Integration\Api\OauthServiceInterface $oauthService,
		\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
	{
		$this->_integrationService = $integrationService;
		$this->_oauthService = $oauthService;
		$this->_eavSetupFactory = $eavSetupFactory;
	}
 
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		$integration = $this->_integrationService->findByName('Custom Gateway Trade Portal');

		if(!$integration->getId())
		{
			$integration = $this
				->_integrationService
				->create([
					'name' => 'Custom Gateway Trade Portal',
					'status' => \Magento\Integration\Model\Integration::STATUS_ACTIVE,
					'all_resources' => true
				]);
		}

		$consumerId = $integration->getConsumerId();

		if(!$this->_oauthService->getAccessToken($consumerId))
		{
			$this->_oauthService->createAccessToken($consumerId, true);
		}

        $eavSetup = $this->_eavSetupFactory->create([ 'setup' => $setup ]);

		$eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, "cg_tp_retailer_ref_id", [
			"type"     => "varchar",
            "backend"  => "",
            "label"    => "CG Trade Portal - Retailer Ref ID",
            "input"    => "text",
            "source"   => "",
            "visible"  => true,
            "required" => false,
            "default" => "",
            "frontend" => "",
            "unique"     => false,
            "note"       => "",
			"system"	=> 0,
		]);

		$eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, "cg_tp_oauth_client_id", [
            "type"     => "varchar",
            "backend"  => "",
            "label"    => "CG Trade Portal - OAuth Client ID",
            "input"    => "text",
            "source"   => "",
            "visible"  => true,
            "required" => false,
            "default" => "",
            "frontend" => "",
            "unique"     => false,
            "note"       => "",
			"system"	=> 0,
        ]);

		$eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, "cg_tp_oauth_client_secret", [
            "type"     => "varchar",
            "backend"  => "",
            "label"    => "CG Trade Portal - OAuth Client Secret",
            "input"    => "text",
            "source"   => "",
            "visible"  => true,
            "required" => false,
            "default" => "",
            "frontend" => "",
            "unique"     => false,
            "note"       => "",
			"system"	=> 0
        ]);
	}
}
